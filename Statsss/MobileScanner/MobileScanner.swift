//
//  MobileScanner.swift
//  Statsss
//
//  Created by Djamil Secco on 22/11/2018.
//  Copyright © 2018 Yesss Group (W) SA. All rights reserved.
//

import Foundation

public struct MobileScanner {
  
  public func login(userId: Int, fullName: String, locationId: Int, locationName: String, roleId: Int, country: String) {
    
    let loginDto = MobileScannerLoginDto.init(userId: userId, fullName: fullName, locationId: locationId, locationName: locationName, roleId: roleId, country: country, device: device, operatingSystem: operatingSystem, operatingSystemVersion: operatingSystemVersion, appVersion: applicationVersion)

    api.mobileScanner.login(parameters: loginDto.toDictionary())
    .then { _ in
      services.log.debug("Mobile Scanner: Log Completed")
    }
    .onError { (error) in
      services.log.error("Mobile Scanner: Log Failed\n\(error)")
    }
    
  }
  
}
