//
//  Services.swift
//  yessselec
//
//  Created by Djamil Secco on 13/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation

class Services {
  
  class var sharedInstance: Services {
    struct Singleton {
      static let instance = Services()
    }
    return Singleton.instance
  }
  
  lazy var log            = LogService().log
  lazy var logNetwork     = LogService().logNetwork
  
}
