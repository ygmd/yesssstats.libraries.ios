//
//  YesssPadService.swift
//  Statsss
//
//  Created by Djamil Secco on 20/11/2018.
//  Copyright © 2018 Yesss Group (W) SA. All rights reserved.
//

import Foundation
import Then

class YesssPadService: BaseService {
  
  func login(parameters: [String:Any]) -> Promise<AnyObject?> {
    let url = "\(baseURL)/login"
    return downloadManager.openUrl(url, httpMethod: .post, parameters: parameters)
  }
  
  func channelViewed(parameters: [String:Any]) -> Promise<AnyObject?> {
    let url = "\(baseURL)/channel/viewed"
    return downloadManager.openUrl(url, httpMethod: .post, parameters: parameters)
  }
  
  func contentViewed(parameters: [String:Any]) -> Promise<AnyObject?> {
    let url = "\(baseURL)/content/viewed"
    return downloadManager.openUrl(url, httpMethod: .post, parameters: parameters)
  }
  
  func contentShared(parameters: [String:Any]) -> Promise<AnyObject?> {
    let url = "\(baseURL)/content/shared"
    return downloadManager.openUrl(url, httpMethod: .post, parameters: parameters)
  }
  
  func contentsShared(parameters: [String:Any]) -> Promise<AnyObject?> {
    let url = "\(baseURL)/content/shared/multiple"
    return downloadManager.openUrl(url, httpMethod: .post, parameters: parameters)
  }
  
}
