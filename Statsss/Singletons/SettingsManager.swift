//
//  SettingsManager.swift
//  yessselec
//
//  Created by Djamil Secco on 13/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import XCGLogger

class SettingsManager {

  class var sharedInstance: SettingsManager {
    struct Singleton {
      static let instance = SettingsManager()
    }
    return Singleton.instance
  }
  
  lazy var currentEnvironment = Environment.staging //.staging //.production
  
  let logLevel: XCGLogger.Level = .debug //.verbose //.debug .info .warning .error .severe .none
  
  // MARK: - APIs Keys
  let kValetKey = "com.statsss.kValetKey"
  let kTokenStoreKey = "com.statsss.TokenStoreKey"
  let kConfigurationStoreKey = "com.statsss.ConfigurationStoreKey"
  
}
