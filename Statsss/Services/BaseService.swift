//
//  BaseService.swift
//  Statsss
//
//  Created by Djamil Secco on 19/02/2019.
//  Copyright © 2019 Yesss Group (W) SA. All rights reserved.
//

import Foundation

class BaseService: NSObject {
  /// The root URL of the API. If nil, then `resource(_:)` will only accept absolute URLs.
  public let baseURL: String
  
  init(baseURL: String) {
    self.baseURL = baseURL
    super.init()
  }
  
}
