//
//  YesssEvents.swift
//  Statsss
//
//  Created by Djamil Secco on 22/11/2018.
//  Copyright © 2018 Yesss Group (W) SA. All rights reserved.
//

import Foundation

public struct YesssEvents {
  
  public func login(contactId: Int, customerId: Int, branchId: Int, regionId: Int, country: String) {
    
    let loginDto = YesssAppLoginDto.init(contactId: contactId, customerId: customerId, branchId: branchId, regionId: regionId, country: country, device: device, operatingSystem: operatingSystem, operatingSystemVersion: operatingSystemVersion, appVersion: applicationVersion)
    
    api.yesssEvents.login(parameters: loginDto.toDictionary())
    .then { _ in
      services.log.debug("Yesss Events: Log Completed")
    }
    .onError { (error) in
      services.log.error("Yesss Events: Log Failed\n\(error)")
    }
    
  }
  
}
