//
//  Token.swift
//  yessselec
//
//  Created by Djamil Secco on 13/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation

struct Token: Codable {
  var accessToken               = String()
  var expiresIn                 = UInt()
  var tokenType                 = String()
  
  enum CodingKeys: String, CodingKey {
    case accessToken            = "access_token"
    case expiresIn              = "expires_in"
    case tokenType              = "token_type"
  }
  
  init() {
  }
  
  init(dictionary:[String: Any]) {
    accessToken                = dictionary["access_token"].toString()
    expiresIn                  = dictionary["expires_in"].toUInt()
    tokenType                  = dictionary["token_type"].toString()
  }
  
  var expiryDate: Date {
    get {
      return Date().plusSeconds(expiresIn - 1)
    }
  }
  
  var userId: String? {
    get {
      let tokenParts = accessToken.components(separatedBy: ".")
      let encodedString = tokenParts[1]
      let padLenght = encodedString.length + ((4 - (encodedString.length % 4)) % 4);
      let paddedEncodedString = encodedString.padding(toLength: padLenght, withPad: "==", startingAt: 0)
      let decodedData = Data.init(base64Encoded: paddedEncodedString, options: .ignoreUnknownCharacters)
      guard let decodedDictionary = decodedData?.jsonDictionary() else { return nil }
      return decodedDictionary["UserId"].toString()
    }
  }
  
  var role: String? {
    get {
      let tokenParts = accessToken.components(separatedBy: ".")
      let encodedString = tokenParts[1]
      let padLenght = encodedString.length + ((4 - (encodedString.length % 4)) % 4);
      let paddedEncodedString = encodedString.padding(toLength: padLenght, withPad: "==", startingAt: 0)
      let decodedData = Data.init(base64Encoded: paddedEncodedString, options: .ignoreUnknownCharacters)
      guard let decodedDictionary = decodedData?.jsonDictionary() else { return nil }
      return decodedDictionary["role"].toString()
    }
  }
  
  var isExpired: Bool {
    get {
      return Date() >= self.expiryDate
    }
  }
  
}
