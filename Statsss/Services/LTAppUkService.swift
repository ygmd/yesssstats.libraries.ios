//
//  LTAppService.swift
//  Extensionsss
//
//  Created by Djamil Secco on 12/11/2018.
//

import Foundation
import Then

class LTAppUkService: BaseService {
  
  func login(parameters: [String:Any]) -> Promise<AnyObject?> {
    let url = "\(baseURL)/login"
    return downloadManager.openUrl(url, httpMethod: .post, parameters: parameters)
  }
  
}
