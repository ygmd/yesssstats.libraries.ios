//
//  StatsssConfiguration.swift
//  Statsss
//
//  Created by Djamil Secco on 07/11/2018.
//  Copyright © 2018 Yesss Group (W) SA. All rights reserved.
//

import Foundation

public struct StatsssConfiguration: Codable {
  var clientId          : String = ""
  var clientSecret      : String = ""
  var isStaging         : Bool   = true
  var scope             : String = "ygmd-stats-api"
  var grantType         : String = "client_credentials"
  
  
  enum CodingKeys: String, CodingKey {
    case clientId       = "client_id"
    case clientSecret   = "client_secret"
    case scope          = "scope"
    case grantType      = "grant_type"
  }
  
  public init(clientId: String, clientSecret: String, isStaging: Bool) {
    self.clientId = clientId
    self.clientSecret = clientSecret
    self.isStaging = isStaging
  }
    
}

