//
//  YesssPad.swift
//  Statsss
//
//  Created by Djamil Secco on 21/11/2018.
//  Copyright © 2018 Yesss Group (W) SA. All rights reserved.
//

import Foundation

public struct YesssPad {
  
  public func login(email: String, firstName: String, lastName: String, organisation: String) {
    let loginDto = YesssPadLoginDto.init(email: email, firstName: firstName, lastName: lastName, organisation: organisation, device: device, operatingSystem: operatingSystem, operatingSystemVersion: operatingSystemVersion, appVersion: applicationVersion)
    api.yesssPad.login(parameters: loginDto.toDictionary())
    .then { _ in
      services.log.debug("YesssPad: Log Completed")
    }
    .onError { (error) in
      services.log.error("YesssPad: Log Failed\n\(error)")
    }
  }
  
  public func channelViewed(id: String, name: String, email: String, firstName: String, lastName: String, organisation: String) {
    let yesssPadChannelDto = YesssPadChannelDto.init(id: id, name: name, email: email, firstName: firstName, lastName: lastName, organisation: organisation, device: device, operatingSystem: operatingSystem, operatingSystemVersion: operatingSystemVersion, appVersion: applicationVersion)
    
    api.yesssPad.channelViewed(parameters: yesssPadChannelDto.toDictionary())
    .then { _ in
      services.log.debug("YesssPad: Log Channel Viewed Completed")
    }
    .onError { (error) in
      services.log.error("YesssPad: Log Channel Viewed Failed \n\(error)")
    }
  }
  
  public func contentViewed(id: String, name: String, email: String, firstName: String, lastName: String, organisation: String) {
    let yesssPadContentDto = YesssPadContentDto.init(id: id, name: name, email: email, firstName: firstName, lastName: lastName, organisation: organisation, device: device, operatingSystem: operatingSystem, operatingSystemVersion: operatingSystemVersion, appVersion: applicationVersion)
    api.yesssPad.contentViewed(parameters: yesssPadContentDto.toDictionary())
    .then { _ in
      services.log.debug("YesssPad: Log Content Viewed Completed")
    }
    .onError { (error) in
      services.log.error("YesssPad: Log Content Viewed Failed \n\(error)")
    }
  }
  
  public func contentShared(id: String, name: String, description: String, email: String, firstName: String, lastName: String, organisation: String) {
    
    let yesssPadContentDto = YesssPadContentSharedDto.init(id: id, name: name, description: description, email: email, firstName: firstName, lastName: lastName, organisation: organisation, device: device, operatingSystem: operatingSystem, operatingSystemVersion: operatingSystemVersion, appVersion: applicationVersion)
    api.yesssPad.contentShared(parameters: yesssPadContentDto.toDictionary())
    .then { _ in
      services.log.debug("YesssPad: Log Content Shared Completed")
    }
    .onError { (error) in
      services.log.error("YesssPad: Log Content Shared Failed \n\(error)")
    }
  }
  
  public func contentsShared(email: String, firstName: String, lastName: String, organisation: String, contentsShared: [[String:String]]) {
    var contentShares = [ContentShare]()
    for content in contentsShared {
      let contentShare = ContentShare.init(id: content["contentId"] ?? "", name: content["contentName"] ?? "", description: content["description"] ?? "")
      contentShares.append(contentShare)
    }
    let yesssPadContentsDto = YesssPadContentsSharedDto.init(email: email, firstName: firstName, lastName: lastName, organisation: organisation, device: device, operatingSystem: operatingSystem, operatingSystemVersion: operatingSystemVersion, appVersion: appVersion, contentShares: contentShares)
    api.yesssPad.contentsShared(parameters: yesssPadContentsDto.toDictionary())
    .then { _ in
      services.log.debug("YesssPad: Log Contents Shared Completed")
    }
    .onError { (error) in
      services.log.error("YesssPad: Log Contents Shared Failed \n\(error)")
    }
  }
  
}
