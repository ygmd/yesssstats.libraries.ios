//
//  YesssPadContentSharedDto.swift
//  Statsss
//
//  Created by Djamil Secco on 21/11/2018.
//  Copyright © 2018 Yesss Group (W) SA. All rights reserved.
//

import Foundation

struct YesssPadContentSharedDto: Codable {
  var id                                    = String()
  var name                                  = String()
  var description                           = String()
  var email                                 = String()
  var firstName                             = String()
  var lastName                              = String()
  var organisation                          = String()
  var device                                = String()
  var operatingSystem                       = String()
  var operatingSystemVersion                = String()
  var appVersion                            = String()
  
  enum CodingKeys: String, CodingKey {
    case id                                 = "contentId"
    case name                               = "contentName"
    case description                        = "description"
    case email                              = "email"
    case firstName                          = "firstName"
    case lastName                           = "lastName"
    case organisation                       = "organisation"
    case device                             = "device"
    case operatingSystem                    = "operatingSystem"
    case operatingSystemVersion             = "operatingSystemVersion"
    case appVersion                         = "appVersion"
  }
  
  func toDictionary() -> [String:Any] {
    var dictionary = [String:Any]()
    dictionary["contentId"]                 = id
    dictionary["contentName"]               = name
    dictionary["description"]               = description
    dictionary["email"]                     = email
    dictionary["firstName"]                 = firstName
    dictionary["lastName"]                  = lastName
    dictionary["organisation"]              = organisation
    dictionary["device"]                    = device
    dictionary["operatingSystem"]           = operatingSystem
    dictionary["operatingSystemVersion"]    = operatingSystemVersion
    dictionary["appVersion"]                = appVersion
    return dictionary
  }
  
}
