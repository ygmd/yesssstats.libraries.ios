//
//  YesssPadChannelDto.swift
//  Statsss
//
//  Created by Djamil Secco on 21/11/2018.
//  Copyright © 2018 Yesss Group (W) SA. All rights reserved.
//

import Foundation

struct YesssPadChannelDto: Codable {
  var id                                    = String()
  var name                                  = String()
  var email                                 = String()
  var firstName                             = String()
  var lastName                              = String()
  var organisation                          = String()
  var device                                = String()
  var operatingSystem                       = String()
  var operatingSystemVersion                = String()
  var appVersion                            = String()
  
  enum CodingKeys: String, CodingKey {
    case id                                 = "channelId"
    case name                               = "channelName"
    case email                              = "email"
    case firstName                          = "firstName"
    case lastName                           = "lastName"
    case organisation                       = "organisation"
    case device                             = "device"
    case operatingSystem                    = "operatingSystem"
    case operatingSystemVersion             = "operatingSystemVersion"
    case appVersion                         = "appVersion"
  }
  
  func toDictionary() -> [String:Any] {
    var dictionary = [String:Any]()
    dictionary["channelId"]                 = id
    dictionary["channelName"]               = name
    dictionary["email"]                     = email
    dictionary["firstName"]                 = firstName
    dictionary["lastName"]                  = lastName
    dictionary["organisation"]              = organisation
    dictionary["device"]                    = device
    dictionary["operatingSystem"]           = operatingSystem
    dictionary["operatingSystemVersion"]    = operatingSystemVersion
    dictionary["appVersion"]                = appVersion
    return dictionary
  }
  
}
