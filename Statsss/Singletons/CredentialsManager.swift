//
//  CredentialsManager.swift
//  yessselec
//
//  Created by Djamil Secco on 13/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import LocalAuthentication
import Valet

class CredentialsManager {
  
  class var sharedInstance: CredentialsManager {
    struct Singleton {
      static let instance = CredentialsManager()
    }
    return Singleton.instance
  }
  
  // MARK: - Keychain Methods
  let keychain = Valet.valet(with: Identifier(nonEmpty: settingsManager.kValetKey)!, accessibility: .whenUnlocked)
  
  var token: Token? {
    get {
      do {
        let jsonData = try keychain.object(forKey: settingsManager.kTokenStoreKey)
        let token = try? JSONDecoder().decode(Token.self, from: jsonData)
        return token
      } catch let error {
        print(error.localizedDescription)
        return nil
      }
    }
  }
  
  func saveToken(token: Token) -> Bool {
    do {
      let jsonData = try JSONEncoder().encode(token)
      try keychain.setObject(jsonData, forKey: settingsManager.kTokenStoreKey)
      return true
    } catch let error {
      print(error.localizedDescription)
      return false
    }
  }
  
  var configuration: StatsssConfiguration? {
    get {
      do {
        let jsonData = try keychain.object(forKey: settingsManager.kConfigurationStoreKey)
        let configuration = try? JSONDecoder().decode(StatsssConfiguration.self, from: jsonData)
        return configuration
      } catch let error {
        print(error.localizedDescription)
        return nil
      }
    }
  }
  
  func saveConfiguration(configuration: StatsssConfiguration) -> Bool {
    do {
      let jsonData = try JSONEncoder().encode(configuration)
      try keychain.setObject(jsonData, forKey: settingsManager.kConfigurationStoreKey)
      return true
    } catch let error {
      print(error.localizedDescription)
      return false
    }
  }
  
  func clearToken() {
    do {
      try keychain.removeObject(forKey: settingsManager.kTokenStoreKey)
    } catch let error {
      print(error.localizedDescription)
    }
  }
  
  func clearData() {
    do {
      try keychain.removeAllObjects()
    } catch let error {
      print(error.localizedDescription)
    }
  }
  
}
