//
//  JWTAccessTokenAdapter.swift
//  Statsss
//
//  Created by Djamil Secco on 19/02/2019.
//  Copyright © 2019 Yesss Group (W) SA. All rights reserved.
//

import Foundation
import Alamofire

final class JWTAccessTokenAdapter: RequestAdapter {
  typealias JWT = String
  private var accessToken: JWT
  
  init(accessToken: JWT) {
    self.accessToken = accessToken
  }
  
  func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
    var urlRequest = urlRequest
    if let urlString = urlRequest.url?.absoluteString, urlString.hasPrefix(baseURL) {
      /// Set the Authorization header value using the access token.
      if let token = credentialsManager.token {
        urlRequest.setValue("\(token.tokenType) \(token.accessToken)", forHTTPHeaderField: "Authorization")
      }
    }
    return urlRequest
  }
}


extension JWTAccessTokenAdapter: RequestRetrier {
  func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
    
    guard let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 else {
      completion(false, 0.0)
      return
    }
    
    api.auth.login(configuration: api.configuration)
    .then { [weak self] _ in
      guard let strongSelf = self, let accessToken = credentialsManager.token?.accessToken else { return }
      strongSelf.accessToken = accessToken
      completion(true, 0.0)
    }
    .onError { (error) in
      services.log.error(error)
      completion(false, 0.0)
    }
  }
}
