//
//  Statsss.swift
//  Statsss
//
//  Created by Djamil Secco on 07/11/2018.
//  Copyright © 2018 Yesss Group (W) SA. All rights reserved.
//

import Foundation

public class Statsss {
  
  class var sharedInstance: Statsss {
    struct Singleton {
      static let instance = Statsss()
    }
    return Singleton.instance
  }
  
  public lazy var log = Log()
  
  public func initialize(configuration: StatsssConfiguration) {
    guard configuration.clientId != "" else {
      services.log.error("Initialization failed. clientId cannot be empty")
      return
    }
    guard configuration.clientSecret != "" else {
      services.log.error("Initialization failed. clientSecret cannot be empty")
      return
    }
    _ = credentialsManager.saveConfiguration(configuration: configuration)
    settingsManager.currentEnvironment = configuration.isStaging ? Environment.staging : Environment.production
  
    services.log.debug("SettingsManager CurrentEnvironment: \(settingsManager.currentEnvironment.description())")
    
    api.updateServices()
    
    api.auth.login(configuration: configuration)
    .then { _X in
      services.log.debug("Initialization complete")
    }
    .onError { error in
      services.log.error(error)
    }
    
  }
  
}

