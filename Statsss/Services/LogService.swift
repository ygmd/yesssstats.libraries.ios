//
//  LogService.swift
//  yessselec
//
//  Created by Djamil Secco on 13/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import XCGLogger

struct LogService {
  
  
  // MARK: - XCGLogger
  let log: XCGLogger = {
    
    let log = XCGLogger.init() //.default
    
    log.setup(level: settingsManager.logLevel, showLogIdentifier: false, showFunctionName: true, showThreadName: true, showLevel: true, showFileNames: true, showLineNumbers: true, showDate: true)
    
    log.logAppDetails()
    
    log.levelDescriptions[.verbose] = "🗯🗯🗯"
    log.levelDescriptions[.debug] = "🔹🔹🔹"
    log.levelDescriptions[.info] = "ℹ️ℹ️ℹ️"
    log.levelDescriptions[.warning] = "⚠️⚠️⚠️"
    log.levelDescriptions[.error] = "‼️‼️‼️"
    log.levelDescriptions[.severe] = "💣💣💣"
    
    let emojiLogFormatter = PrePostFixLogFormatter()
    emojiLogFormatter.apply(prefix: "🗯🗯🗯 ", postfix: " 🗯🗯🗯", to: .verbose)
    emojiLogFormatter.apply(prefix: "🔹🔹🔹 ", postfix: " 🔹🔹🔹", to: .debug)
    emojiLogFormatter.apply(prefix: "ℹ️ℹ️ℹ️ ", postfix: " ℹ️ℹ️ℹ️", to: .info)
    emojiLogFormatter.apply(prefix: "⚠️⚠️⚠️ ", postfix: " ⚠️⚠️⚠️", to: .warning)
    emojiLogFormatter.apply(prefix: "‼️‼️‼️ ", postfix: " ‼️‼️‼️", to: .error)
    emojiLogFormatter.apply(prefix: "💣💣💣 ", postfix: " 💣💣💣", to: .severe)
    log.formatters = [emojiLogFormatter]
    log.logAppDetails()
    
    return log
    
  }()
  
  let logNetwork: XCGLogger = {
    let log = XCGLogger.init() //.default
    log.setup(level: settingsManager.logLevel, showLogIdentifier: false, showFunctionName: false, showThreadName: false, showLevel: false, showFileNames: false, showLineNumbers: false, showDate: true)
    log.levelDescriptions[.debug] = "🚦🚦🚦"
    let emojiLogFormatter = PrePostFixLogFormatter()
    emojiLogFormatter.apply(prefix: "🚦🚦🚦 ", postfix: " 🚦🚦🚦", to: .debug)
    log.formatters = [emojiLogFormatter]
    log.logAppDetails()
    return log
  }()

}
