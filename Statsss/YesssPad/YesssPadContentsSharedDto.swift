//
//  YesssPadContentsSharedDto.swift
//  Statsss
//
//  Created by Djamil Secco on 08/01/2019.
//  Copyright © 2019 Yesss Group (W) SA. All rights reserved.
//

import Foundation

struct ContentShare: Codable {
  var id                                    = String()
  var name                                  = String()
  var description                           = String()
  
  enum CodingKeys: String, CodingKey {
    case id                                 = "contentId"
    case name                               = "contentName"
    case description                        = "description"
  }
  
  func toDictionary() -> [String:Any] {
    var dictionary = [String:Any]()
    dictionary["contentId"]                 = id
    dictionary["contentName"]               = name
    dictionary["description"]               = description
    return dictionary
  }
}

struct YesssPadContentsSharedDto: Codable {
  var email                                 = String()
  var firstName                             = String()
  var lastName                              = String()
  var organisation                          = String()
  var device                                = String()
  var operatingSystem                       = String()
  var operatingSystemVersion                = String()
  var appVersion                            = String()
  var contentShares                         = [ContentShare]()
  
  enum CodingKeys: String, CodingKey {
    case email                              = "email"
    case firstName                          = "firstName"
    case lastName                           = "lastName"
    case organisation                       = "organisation"
    case device                             = "device"
    case operatingSystem                    = "operatingSystem"
    case operatingSystemVersion             = "operatingSystemVersion"
    case appVersion                         = "appVersion"
    case contentShares                      = "contentShares"
  }
  
  func toDictionary() -> [String:Any] {
    var dictionary = [String:Any]()
    dictionary["email"]                     = email
    dictionary["firstName"]                 = firstName
    dictionary["lastName"]                  = lastName
    dictionary["organisation"]              = organisation
    dictionary["device"]                    = device
    dictionary["operatingSystem"]           = operatingSystem
    dictionary["operatingSystemVersion"]    = operatingSystemVersion
    dictionary["appVersion"]                = appVersion
    dictionary["contentShares"]             = contentShares.map {$0.toDictionary()}
    return dictionary
  }
  
}
