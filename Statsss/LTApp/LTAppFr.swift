//
//  LTAppFr.swift
//  Statsss
//
//  Created by Djamil Secco on 21/11/2018.
//  Copyright © 2018 Yesss Group (W) SA. All rights reserved.
//

import Foundation

public struct LTAppFr {
  
  let country = "FR"
  
  public func login(name: String, role:String, userId: Int, branchId: Int, branchName:String, regionId: Int, regionName:String) {
    
    let loginDto = LTAppLoginDto.init(name: name, role: role, userId: userId, branchId: branchId, branchName: branchName, regionId: regionId, regionName: regionName, country: country, device: device, operatingSystem: operatingSystem, operatingSystemVersion: operatingSystemVersion, appVersion: applicationVersion)
    
    api.ltappFr.login(parameters: loginDto.toDictionary())
    .then { _ in
      services.log.debug("LTApp: Log Completed")
    }
    .onError { (error) in
      services.log.error("LTApp: Log Failed\n\(error)")
    }
    
  }
  
}
