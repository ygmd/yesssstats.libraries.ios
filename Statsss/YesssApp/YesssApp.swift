//
//  YesssApp.swift
//  Extensionsss
//
//  Created by Djamil Secco on 12/11/2018.
//

import Foundation

public struct YesssApp {
  
  public func login(contactId: Int, customerId: Int, branchId: Int, regionId: Int, country: String) {
    
    let loginDto = YesssAppLoginDto.init(contactId: contactId, customerId: customerId, branchId: branchId, regionId: regionId, country: country, device: device, operatingSystem: operatingSystem, operatingSystemVersion: operatingSystemVersion, appVersion: applicationVersion)
    
    api.yesssApp.login(parameters: loginDto.toDictionary())
    .then { _ in
      services.log.debug("Yesss App: Log Completed")
    }
    .onError { (error) in
      services.log.error("Yesss App: Log Failed\n\(error)")
    }
    
  }
  
}
