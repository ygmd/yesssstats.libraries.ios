//
//  Log.swift
//  Extensionsss
//
//  Created by Djamil Secco on 12/11/2018.
//

import Foundation

public struct Log {
  
  public lazy var customerApp   = YesssApp()
  public lazy var ltappUk       = LTAppUk()
  public lazy var ltappDe       = LTAppDe()
  public lazy var ltappIt       = LTAppIt()
  public lazy var ltappNl       = LTAppNl()
  public lazy var ltappFr       = LTAppFr()
  public lazy var yesssPad      = YesssPad()
  public lazy var mobileScanner = MobileScanner()
  public lazy var yesssEvents   = YesssEvents()
  
}
