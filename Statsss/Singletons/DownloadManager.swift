//
//  DownloadManager.swift
//  Statsss
//
//  Created by Djamil Secco on 19/02/2019.
//  Copyright © 2019 Yesss Group (W) SA. All rights reserved.
//

import Foundation
import Alamofire
import Then

class DownloadManager {
  
  class var sharedInstance: DownloadManager {
    struct Singleton {
      static let instance = DownloadManager()
    }
    return Singleton.instance
  }
  
  let sessionManager = SessionManager()
  
  func downloadJsonFromUrl(_ url: String, httpMethod: HTTPMethod = .get, parameters: Parameters? = nil, isDownloadingToken: Bool = false) -> Promise<AnyObject?> {
    return Promise { resolve, reject in
      //var encoding :ParameterEncoding =  httpMethod == .get ? URLEncoding.default : JSONEncoding.default
      var encoding :ParameterEncoding = URLEncoding.default
      self.sessionManager.adapter = JWTAccessTokenAdapter(accessToken: credentialsManager.token?.accessToken ?? "")
      var headers = api.headers
      if isDownloadingToken {
        encoding = URLEncoding.default
        headers = api.headersAuth
      }
      self.sessionManager
        .request(url, method: httpMethod, parameters: parameters, encoding: encoding, headers: headers)
        .validate(statusCode: 200..<300)
        .responseJSON { response in
          self.logResponse(response)
          switch response.result {
          case .success:
            services.log.debug("Validation Successful")
            guard let json = response.result.value else { services.log.debug("No Data") ; resolve(nil) ; return }
            services.log.debug("JSON: \(json)")
            resolve(json as AnyObject)
          case .failure(let error):
            // Print out reponse body
            
            let sc = response.response?.statusCode
            
            services.log.error(sc)
            
            let wsError = YWSError.init(httpStatusCode: response.response?.statusCode ?? -1)
            guard let data = response.data, let responseString = String(data: data, encoding: .utf8) else { reject(wsError) ;  return }
            services.log.error("****** response data = \(responseString)") // original server data as UTF8 string
            services.log.error(error)
            reject(wsError)
          }
      }
    }
  }
  
  func openUrl(_ url: String, httpMethod: HTTPMethod = .get, parameters: Parameters? = nil) -> Promise<AnyObject?> {
    return Promise { resolve, reject in
      let encoding :ParameterEncoding =  httpMethod == .get ? URLEncoding.default : JSONEncoding.default
      self.sessionManager.adapter = JWTAccessTokenAdapter(accessToken: credentialsManager.token?.accessToken ?? "")
      let headers = api.headers
      self.sessionManager
        .request(url, method: httpMethod, parameters: parameters, encoding: encoding, headers: headers)
        .validate(statusCode: 200..<300)
        .responseData { response in
          self.logResponseData(response)
          switch response.result {
          case .success:
            services.log.debug("Validation Successful")
            guard let json = response.result.value else { services.log.debug("No Data") ; resolve(nil) ; return }
            services.log.debug("JSON: \(json)")
            resolve(json as AnyObject)
          case .failure(let error):
            // Print out reponse body
            
            let sc = response.response?.statusCode
            
            services.log.error(sc)
            
            let wsError = YWSError.init(httpStatusCode: response.response?.statusCode ?? -1)
            guard let data = response.data, let responseString = String(data: data, encoding: .utf8) else { reject(wsError) ;  return }
            services.log.error("****** response data = \(responseString)") // original server data as UTF8 string
            services.log.error(error)
            reject(wsError)
          }
        }
    }
  }
  
  // MARK: - HELPER METHODS
  func logResponse(_ response: DataResponse<Any>) {
    services.log.debug("Request: \(String(describing: response.request))")   // original url request
    services.log.debug("Response: \(String(describing: response.response))") // http url response
    services.log.debug("Result: \(response.result)")                         // response serialization result
    services.log.debug(response.data)                                         // server data
    services.log.debug(response.timeline)                                     // response timeline
    services.log.debug(response.metrics)                                      // response metrics
  }
  
  func logResponseData(_ response: DataResponse<Data>) {
    services.log.debug("Request: \(String(describing: response.request))")   // original url request
    services.log.debug("Response: \(String(describing: response.response))") // http url response
    services.log.debug("Result: \(response.result)")                         // response serialization result
    services.log.debug(response.data)                                         // server data
    services.log.debug(response.timeline)                                     // response timeline
    services.log.debug(response.metrics)                                      // response metrics
  }
  
}
