//
//  LTAppLoginDto.swift
//  Extensionsss
//
//  Created by Djamil Secco on 12/11/2018.
//

import Foundation

struct LTAppLoginDto: Codable {
  var name                                    = String()
  var role                                    = String()
  var userId                                  = Int()
  var branchId                                = Int()
  var branchName                              = String()
  var regionId                                = Int()
  var regionName                              = String()
  var country                                 = String()
  var device                                  = String()
  var operatingSystem                         = String()
  var operatingSystemVersion                  = String()
  var appVersion                              = String()
  
  enum CodingKeys: String, CodingKey {
    case name                                 = "name"
    case role                                 = "role"
    case userId                               = "yesssUserId"
    case branchId                             = "yesssBranchLocationId"
    case branchName                           = "yesssBranchLocationName"
    case regionId                             = "yesssRegionLocationId"
    case regionName                           = "yesssRegionLocationName"
    case country                              = "country"
    case device                               = "device"
    case operatingSystem                      = "operatingSystem"
    case operatingSystemVersion               = "operatingSystemVersion"
    case appVersion                           = "appVersion"
  }
  
  func toDictionary() -> [String:Any] {
    var dictionary = [String:Any]()
    dictionary["name"]                        = name
    dictionary["role"]                        = role
    dictionary["yesssUserId"]                 = userId
    dictionary["yesssBranchLocationId"]       = branchId
    dictionary["yesssBranchLocationName"]     = branchName
    dictionary["yesssRegionLocationId"]       = regionId
    dictionary["yesssRegionLocationName"]     = regionName
    dictionary["country"]                     = country
    dictionary["device"]                      = device
    dictionary["operatingSystem"]             = operatingSystem
    dictionary["operatingSystemVersion"]      = operatingSystemVersion
    dictionary["appVersion"]                  = appVersion
    return dictionary
  }
  
}
