//
//  Optional+Extensions.swift
//  Statsss
//
//  Created by Djamil Secco on 25/03/2024.
//  Copyright © 2024 Yesss Group (W) SA. All rights reserved.
//

import Foundation

public extension Optional {
  
  func isNull(someObject: Any?) -> Bool {
    guard let someObject = someObject else {
      return true
    }
    guard "\(someObject)" != "<null>" else {
      return true
    }
    return (someObject is NSNull)
  }
  
  func toBool() -> Bool {
    guard let value = self else { return false }
    return ["1", "y", "true"].contains("\(value)".lowercased())
  }
  
  func toString() -> String {
    guard let value = self else { return "" }
    return "\(value)"
  }
  
  func toInt() -> Int {
    guard let value = self else { return 0 }
    return "\(value)".toInt()
  }
  
  func toInt(locale: Locale) -> Int {
    guard let value = self else { return 0 }
    let numberformatter = NumberFormatter()
    numberformatter.locale = locale
    numberformatter.numberStyle = .decimal
    guard let numberFromNumberFormatter = numberformatter.number(from: "\(value)") else {
      return 0
    }
    return numberFromNumberFormatter.intValue
  }
  
  func toUInt() -> UInt {
    guard let value = self else { return 0 }
    return "\(value)".toUInt()
  }
  
  func toUInt(locale: Locale) -> UInt {
    guard let value = self else { return 0 }
    let numberformatter = NumberFormatter()
    numberformatter.locale = locale
    numberformatter.numberStyle = .decimal
    guard let numberFromNumberFormatter = numberformatter.number(from: "\(value)") else {
      return 0
    }
    return numberFromNumberFormatter.uintValue
  }
  
  func toDouble(fractionDigits: Int?=nil) -> Double {
    guard let value = self else { return 0 }
    return "\(value)".toDouble(fractionDigits: fractionDigits)
  }
  
  func toDouble(locale: Locale, fractionDigits: Int?=nil) -> Double {
    guard let value = self else { return 0 }
    let numberformatter = NumberFormatter()
    numberformatter.locale = locale
    numberformatter.numberStyle = .decimal
    numberformatter.minimumFractionDigits = fractionDigits ?? 2
    numberformatter.maximumFractionDigits = fractionDigits ?? 2
    guard let numberFromNumberFormatter = numberformatter.number(from: "\(value)") else {
      return 0
    }
    return numberFromNumberFormatter.doubleValue
  }
  
  func toFloat(fractionDigits: Int?=nil) -> Float {
    guard let value = self else { return 0 }
    return "\(value)".toFloat(fractionDigits: fractionDigits)
  }
  
  func toFloat(locale: Locale, fractionDigits: Int?=nil) -> Float {
    guard let value = self else { return 0 }
    let numberformatter = NumberFormatter()
    numberformatter.locale = locale
    numberformatter.numberStyle = .decimal
    numberformatter.minimumFractionDigits = fractionDigits ?? 2
    numberformatter.maximumFractionDigits = fractionDigits ?? 2
    guard let numberFromNumberFormatter = numberformatter.number(from: "\(value)") else {
      return 0
    }
    return numberFromNumberFormatter.floatValue
  }
  
  func toDictionary() -> [String:Any] {
    return self as? [String:Any] ?? [String:Any]()
  }

}
