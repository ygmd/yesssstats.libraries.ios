//
//  YesssAppLoginDto.swift
//  Extensionsss
//
//  Created by Djamil Secco on 12/11/2018.
//

import Foundation

struct YesssAppLoginDto: Codable {
  var contactId                             = Int()
  var customerId                            = Int()
  var branchId                              = Int()
  var regionId                              = Int()
  var country                               = String()
  var device                                = String()
  var operatingSystem                       = String()
  var operatingSystemVersion                = String()
  var appVersion                            = String()
  
  enum CodingKeys: String, CodingKey {
    case contactId                          = "yesssCustomerContactId"
    case customerId                         = "yesssCustomerId"
    case branchId                           = "yesssBranchLocationId"
    case regionId                           = "yesssRegionLocationId"
    case country                            = "country"
    case device                             = "device"
    case operatingSystem                    = "operatingSystem"
    case operatingSystemVersion             = "operatingSystemVersion"
    case appVersion                         = "appVersion"
  }
  
  func toDictionary() -> [String:Any] {
    var dictionary = [String:Any]()
    dictionary["yesssCustomerContactId"]    = contactId
    dictionary["yesssCustomerId"]           = customerId
    dictionary["yesssBranchLocationId"]     = branchId
    dictionary["yesssRegionLocationId"]     = regionId
    dictionary["country"]                   = country
    dictionary["device"]                    = device
    dictionary["operatingSystem"]           = operatingSystem
    dictionary["operatingSystemVersion"]    = operatingSystemVersion
    dictionary["appVersion"]                = appVersion
    return dictionary
  }
  
}
