//
//  MobileScannerService.swift
//  Statsss
//
//  Created by Djamil Secco on 22/11/2018.
//  Copyright © 2018 Yesss Group (W) SA. All rights reserved.
//

import Foundation
import Then

class MobileScannerService: BaseService {
  
  func login(parameters: [String:Any]) -> Promise<AnyObject?> {
    let url = "\(baseURL)/login"
    return downloadManager.openUrl(url, httpMethod: .post, parameters: parameters)
  }
  
}
