//
//  MobileScannerLoginDto.swift
//  Statsss
//
//  Created by Djamil Secco on 22/11/2018.
//  Copyright © 2018 Yesss Group (W) SA. All rights reserved.
//

import Foundation

struct MobileScannerLoginDto: Codable {
  var userId                                  = Int()
  var fullName                                = String()
  var locationId                              = Int()
  var locationName                            = String()
  var roleId                                  = Int()
  var country                                 = String()
  var device                                  = String()
  var operatingSystem                         = String()
  var operatingSystemVersion                  = String()
  var appVersion                              = String()
  
  enum CodingKeys: String, CodingKey {
    case userId                               = "userId"
    case fullName                             = "fullName"
    case locationId                           = "locationId"
    case locationName                         = "locationName"
    case roleId                               = "roleId"
    case country                              = "country"
    case device                               = "device"
    case operatingSystem                      = "operatingSystem"
    case operatingSystemVersion               = "operatingSystemVersion"
    case appVersion                           = "appVersion"
  }
  
  func toDictionary() -> [String:Any] {
    var dictionary = [String:Any]()
    dictionary["userId"]                      = userId
    dictionary["fullName"]                    = fullName
    dictionary["locationId"]                  = locationId
    dictionary["locationName"]                = locationName
    dictionary["roleId"]                      = roleId
    dictionary["country"]                     = country
    dictionary["device"]                      = device
    dictionary["operatingSystem"]             = operatingSystem
    dictionary["operatingSystemVersion"]      = operatingSystemVersion
    dictionary["appVersion"]                  = appVersion
    return dictionary
  }
  
}
