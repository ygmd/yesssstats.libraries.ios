//
//  AuthService.swift
//  yessselec
//
//  Created by Djamil Secco on 12/09/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation
import Then

class AuthService: BaseService {
  
  
 func login(configuration: StatsssConfiguration) -> Promise<AnyObject?> { //@discardableResult
    return Promise { resolve, reject in
      var parameters = [String:String]()
      parameters["client_id"] = configuration.clientId
      parameters["client_secret"] = configuration.clientSecret
      parameters["scope"] = configuration.scope
      parameters["grant_type"] = configuration.grantType
      let url = "\(self.baseURL)/token"
      downloadManager.downloadJsonFromUrl(url, httpMethod: .post, parameters: parameters, isDownloadingToken: true)
      .then { json in
        guard let jsonDictionary = json as? [String: Any] else { reject(YWSError.init(status: .badFormat)) ; return }
        let token = Token.init(dictionary: jsonDictionary)
        guard credentialsManager.saveToken(token: token) else { reject(YWSError.init(status: .failedToSaveInKeychain)) ; return }
        guard credentialsManager.saveConfiguration(configuration: configuration) else { reject(YWSError.init(status: .failedToSaveInKeychain)) ; return }
        resolve(nil)
      }
      .onError { error in
        reject(error)
      }
    }
  }

}
