Pod::Spec.new do |s|

  s.name            = "Statsss"
  s.version         = "3.7.5"
  s.summary         = "Library to log stats for Yesss Apps."
  s.description     = "Library to log stats for Yesss Applications."
  s.homepage        = "https://djsec@bitbucket.org/ygmd/yesssstats.libraries.ios"
  s.license         = { :type => 'MIT', :file => 'LICENSE.txt' }
  s.author          = { "Djamil Secco" => "ds@yesss.lu" }
  s.platform        = :ios, "11.4"
  s.source          = { :git => "https://djsec@bitbucket.org/ygmd/yesssstats.libraries.ios.git", :tag => "#{s.version}" }
  s.source          = { :path => '.' }
  s.source_files    = "Statsss/*/**"
  s.dependency      'Alamofire', '~> 4.9.1'
  s.dependency      'XCGLogger', '~> 7.0'
  s.dependency      'Valet'
  s.dependency      'thenPromise'
  s.swift_version   = "5"

end
