//
//  Constants.swift
//  yessselec
//
//  Created by Djamil Secco on 13/07/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import UIKit
import SystemConfiguration


// MARK: - Current device
let currentDevice = UIDevice.current
let currentDeviceModel = UIDevice.current.model
let currentLanguageDict = Locale.components(fromIdentifier: Locale.preferredLanguages.first!)
let currentCountryCode = Locale.current.regionCode!
let currentLanguageCode = Locale.current.languageCode!
let iOSVersion = UIDevice.current.systemVersion

let device = UIDevice.current.model
let operatingSystem = UIDevice.current.systemName
let operatingSystemVersion = UIDevice.current.systemVersion
let applicationVersion = "\(application.applicationVersion()).\(application.applicationBuild())"

// MARK: - Shared instances
let application = UIApplication.shared
let defaultSession = URLSession.shared
let defaults = UserDefaults.standard
let settingsManager = SettingsManager.sharedInstance
let services = Services.sharedInstance
let credentialsManager = CredentialsManager.sharedInstance
let downloadManager = DownloadManager.sharedInstance
public let statsss = Statsss.sharedInstance

// MARK: - Simulator
let isRunningOnSimulator: Bool = {
  var isSim = false
  #if arch(i386) || arch(x86_64)
  isSim = true
  #endif
  return isSim
}()

// MARK: Zeros
let CGPointZero = CGPoint(x: 0, y: 0)
let CGSizeZero = CGSize(width: 0, height: 0)

// MARK: - App
let appName: String = {
  guard let appName = Bundle.main.infoDictionary?["CFBundleDisplayName"] as? String else {
    return "N/A"
  }
  return appName
}()

let appVersion: String = {
  guard let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
    return "N/A"
  }
  return appVersion
}()

let appBuild: String = {
  guard let appVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String else {
    return "N/A"
  }
  return appVersion
}()

// MARK: - Network
func isNetworkOk(sender: UIViewController) -> Bool {
  guard isDeviceConnectedToNetwork else {
    services.log.error("No Wireless or 3G Connection")
    return false
  }
  services.log.debug("Internet connection ok")
  return true
}

var isDeviceConnectedToNetwork: Bool {
  get {
    var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
      $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
        SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
      }
    }
    
    var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
    
    guard SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) else {
      return false
    }
    
    // Working for Cellular and WIFI
    let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
    let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
    
    return (isReachable && !needsConnection)
  }
}

// MARK: - Colors
let green = UIColor.green
let red = UIColor.red
let orange = UIColor.orange
let yellow = UIColor.yellow
let blue = UIColor.blue
let black = UIColor.black
let white = UIColor.white
let purple = UIColor.purple
let magenta = UIColor.magenta
let cyan = UIColor.cyan
let brown = UIColor.brown
let gray = UIColor.gray
let clear = UIColor.clear
let yesssBlue = UIColor(red: 0.0 / 255.0, green: 156.0 / 255.0, blue: 255.0 / 255.0, alpha: 1)
let yesssYellow = UIColor(red: 255.0 / 255.0, green: 228.0 / 255.0, blue: 0.0 / 255.0, alpha: 1)
let yesssGreen = UIColor(red: 138.0 / 255.0, green: 204.0 / 255.0, blue: 17.0 / 255.0, alpha: 1)
let gold = UIColor(red: 212.0 / 255.0, green: 175.0 / 255.0, blue: 55.0 / 255.0, alpha: 1)
let silver = UIColor(red: 192.0 / 255.0, green: 192.0 / 255.0, blue: 192.0 / 255.0, alpha: 1)
let bronze = UIColor(red: 80.0 / 255.0, green: 50.0 / 255.0, blue: 20.0 / 255.0, alpha: 1)

// MARK: - Enums
enum AlertTheme: Int {
  case dark   = 0
  case light  = 1
}

// MARK: Country / Environment
enum Environment: String {
  case staging
  case production
  
  func abbreviated() -> String {
    switch self {
    case .staging:
      return "S"
    case .production:
      return "P"
    }
  }
  
  func description() -> String {
    switch self {
    case .staging:
      return "Staging"
    case .production:
      return "Production"
    }
  }
  
}
