//
//  API.swift
//  yessselec
//
//  Created by Djamil Secco on 11/09/2018.
//  Copyright © 2018 YESSS GROUP (W) SA. All rights reserved.
//

import Foundation

// MARK: - URLs
var tokenURL: String {
  get {
    return settingsManager.currentEnvironment == .production ? "https://ygmd-auth.azurewebsites.net/connect" : "https://ygmd-auth-staging.azurewebsites.net/connect"
  }
}

var baseURL: String {
  get {
    return settingsManager.currentEnvironment == .production ? "https://stats-api.azurewebsites.net" : "https://stats-api-staging.azurewebsites.net"
  }
}

var api = Api.sharedInstance

struct Api {
  static let sharedInstance = Api()
  
  // MARK: - Headers
  var headersAuth: [String : String] {
    get {
      var headersDict = [String : String]()
      headersDict["Content-Type"] = "application/x-www-form-urlencoded"
      return headersDict
    }
  }
  
  var headers: [String : String] {
    get {
      var headersDict = [String : String]()
      headersDict["Content-Type"] = "application/json"
      headersDict["language"] = currentLanguageCode == "fr" ? "fr" : "eng"
      return headersDict
    }
  }
  
  var configuration: StatsssConfiguration {
    get {
      guard let configuration = credentialsManager.configuration else { return StatsssConfiguration.init(clientId: "", clientSecret: "", isStaging: true)}
      return configuration
    }
  }
  
  // MARK: - Services
  private(set) lazy var auth              = AuthService(baseURL: tokenURL)
  private(set) lazy var yesssApp          = YesssAppService(baseURL: "\(baseURL)/customerapp")
  private(set) lazy var ltappUk           = LTAppUkService(baseURL: "\(baseURL)/ltapp/uk")
  private(set) lazy var ltappDe           = LTAppDeService(baseURL: "\(baseURL)/ltapp/de")
  private(set) lazy var ltappIt           = LTAppItService(baseURL: "\(baseURL)/ltapp/it")
  private(set) lazy var ltappNl           = LTAppNlService(baseURL: "\(baseURL)/ltapp/nl")
  private(set) lazy var ltappFr           = LTAppFrService(baseURL: "\(baseURL)/ltapp/fr")
  private(set) lazy var yesssPad          = YesssPadService(baseURL: "\(baseURL)/yessspad")
  private(set) lazy var mobileScanner     = MobileScannerService(baseURL: "\(baseURL)/scanner")
  private(set) lazy var yesssEvents       = YesssEventsService(baseURL: "\(baseURL)/events")
  
  mutating func updateServices() {
    auth              = AuthService(baseURL: tokenURL)
    yesssApp          = YesssAppService(baseURL: "\(baseURL)/customerapp")
    ltappUk           = LTAppUkService(baseURL: "\(baseURL)/ltapp/uk")
    ltappDe           = LTAppDeService(baseURL: "\(baseURL)/ltapp/de")
    ltappIt           = LTAppItService(baseURL: "\(baseURL)/ltapp/it")
    ltappNl           = LTAppNlService(baseURL: "\(baseURL)/ltapp/nl")
    ltappFr           = LTAppFrService(baseURL: "\(baseURL)/ltapp/fr")
    yesssPad          = YesssPadService(baseURL: "\(baseURL)/yessspad")
    mobileScanner     = MobileScannerService(baseURL: "\(baseURL)/scanner")
    yesssEvents       = YesssEventsService(baseURL: "\(baseURL)/events")
  }
  
  private init() { }
  
}
